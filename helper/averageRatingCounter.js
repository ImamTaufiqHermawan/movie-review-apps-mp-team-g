const { newReview } = require('../models');

module.exports = async (id) => {
    const data = await newReview.findAndCountAll({
        where: { movieId: id }
    })
    let totalRating = 0;
    data.rows.forEach(i => {
        totalRating += i.rating;
    });
    if (!totalRating) {
        return 0
    } else {
        return Math.round(totalRating / data.count);
    }
}

// const { newReview } = require('../models');

// module.exports = async (id) => {
//     let getAverage = 0
//     let totalRating = 0

//     const rate = await newReview.findAndCountAll({
//         attributes: ['rating'],
//         where: {
//             movieId: id
//         }
//     })

//     if (rate.count > 0) {
//         rate.rows.forEach(element => {
//             if (element.rating === null) element.rating = 0
//             getAverage = getAverage + element.rating
//         });
//         getAverage = getAverage / rate.count
//         totalRating = getAverage.toFixed(1)
//     }
//     // console.log(rating);
//     return parseInt(totalRating)
// }
