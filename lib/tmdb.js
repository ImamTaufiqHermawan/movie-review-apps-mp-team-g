const axios = require("axios");

const TMDB_APIKEY = `api_key=${process.env.TMDB_API_KEY}`;
const TMDB_URL = `https://api.themoviedb.org/3/`;

const getMovies = async function (page) {
  const response = await axios.get(
    `${TMDB_URL}movie/popular?${TMDB_APIKEY}&page=${page}&without_genres=10749`);
  return response.data;
};

const getMovie = async function (movie_id) {
  const response = await axios.get(
    `${TMDB_URL}movie/${movie_id}?${TMDB_APIKEY}`);
  return response.data;
};

const searchMovie = async function (query, page) {
  const response = await axios.get(
    `${TMDB_URL}search/movie?query=${query}&page=${page}&${TMDB_APIKEY}`
  );
  return response.data;
};

const getByGenre = async function (genre_id, page) {
  const response = await axios.get(
    `${TMDB_URL}discover/movie?${TMDB_APIKEY}&with_genres=${genre_id}&page=${page}`
  )
  return response.data;
}

const getCredits = async function (movie_id) {
  const response = await axios.get(
    `${TMDB_URL}movie/${movie_id}/credits?${TMDB_APIKEY}`
  )
  return response.data
}




module.exports = {
  getMovies,
  getMovie,
  searchMovie,
  getByGenre,
  getCredits
};
