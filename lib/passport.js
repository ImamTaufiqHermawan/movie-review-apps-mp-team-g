const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const { user } = require('../models')

async function authenticate(email, password, done) {
  try {
    const User = await user.authenticate({ email, password })
    return done(null, User)
  }

  catch(err) {
    return done(null, false, { message: err.message })
  }
} 

passport.use(
  new LocalStrategy({ usernameField: 'email' }, authenticate)
)

/* Serialize and Deserialize
 * It means, how would you store and remove the object
 * From the session
 * */
passport.serializeUser(
  (User, done) => done(null, User.id)
)
passport.deserializeUser(
  async (id, done) => done(null, await user.findByPk(id))
)

module.exports = passport