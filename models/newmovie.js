'use strict';
module.exports = (sequelize, DataTypes) => {
  const newMovie = sequelize.define('newMovie', {
    movieId: DataTypes.STRING,
    averageRating: DataTypes.INTEGER
  }, {});
  newMovie.associate = function(models) {
    // associations can be defined here
  };
  return newMovie;
};