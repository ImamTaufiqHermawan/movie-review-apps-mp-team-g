'use strict';
module.exports = (sequelize, DataTypes) => {
  const review = sequelize.define('review', {
    description: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    rating: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    userId: DataTypes.INTEGER,
    movieId: DataTypes.INTEGER
  }, {});
  review.associate = function (models) {
    review.belongsTo(models.user);
    review.belongsTo(models.movie);
  };
  return review;
};