'use strict';
module.exports = (sequelize, DataTypes) => {
  const movie = sequelize.define('movie', {
    title: DataTypes.STRING,
    tmdb_id: DataTypes.INTEGER,
    image_url: DataTypes.STRING,
    synopsis: DataTypes.TEXT,
    genre: DataTypes.STRING,
    backdrop_url: DataTypes.STRING,
    productionCompany: DataTypes.STRING,
    movieDuration: DataTypes.INTEGER,
    releaseDate: DataTypes.STRING,
    status: DataTypes.STRING,
    originalLanguage: DataTypes.STRING,
    budget: DataTypes.STRING,
    revenue: DataTypes.STRING,
    averageRating: DataTypes.INTEGER,
  }, {});
  movie.associate = function (models) {
    movie.hasMany(models.review)
  };
  Object.defineProperty(movie, "getter", {
    get() {
      return {
        title: this.title,
        tmdb_id: this.tmdb_id,
        poster: this.image_url,
        synopsis: this.synopsis,
        genre: this.genre,
        backdrop_url: this.backdrop_url,
        productionCompany: this.productionCompany,
        movieDuration: this.movieDuration,
        releaseDate: this.releaseDate,
        status: this.status,
        originalLanguage: this.originalLanguage,
        budget: this.budget,
        revenue: this.revenue,
        averageRating: this.averageRating,
      };
    },
  });
  return movie;
};
