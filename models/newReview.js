'use strict';
module.exports = (sequelize, DataTypes) => {
  const revieww = sequelize.define('newReview', {
    title: {
      type: DataTypes.STRING
    },
    description: {
      type: DataTypes.TEXT
    },
    rating: {
      type: DataTypes.INTEGER,
      validate: {
        isNumeric: {
          args: true,
          msg: "Please Input Numeric"
        },
        max: {
          args: 10,
          msg: "Greater than 10"
        },
        min: {
          args: 1,
          msg: "Lesser than 1"
        }
      },
      allowNull: false
    },
    userId: DataTypes.INTEGER,
    movieId: DataTypes.INTEGER
  }, { tableName: 'reviewws' });
  revieww.associate = function (models) {
    revieww.belongsTo(models.user);
  };
  return revieww;
};