const bcrypt = require('bcryptjs');

'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: {
        args: true,
        msg: 'Email already registered'
      },
      validate: {
        isLowercase: true,
        isEmail: {
          msg: 'Email is invalid'
        }
      }
    },
    encrypted_password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    role: {
      type: DataTypes.ENUM('admin', 'member'),
      defaultValue: 'member',
      allowNull: false
    }
  }, {});
  Object.defineProperty(user.prototype, 'entity', {
    get() {
      return {
        id: this.id,
        email: this.email,
      }
    }
  })

  // "Private" static _encrypt
  user._encrypt = password => bcrypt.hashSync(password, 10)

  // Public static register
  user.register = async function({ email, password }) {
    return this.create({ email, encrypted_password: this._encrypt(password) })
  }

  user.prototype.verifyPassword = function(password) {
    return bcrypt.compareSync(password, this.encrypted_password)
  }

  user.authenticate = async function({ email, password }) {
    const instance = await this.findOne({
      where: { email }
    })

    if (!instance) return Promise.reject(new Error('Email hasn\'t registered'))
    if (!instance.verifyPassword(password))
      return Promise.reject(new Error('Wrong password'))

    return Promise.resolve(instance)
  }

  user.associate = function (models) {
    user.hasOne(models.profile, {
      foreignKey: 'user_id'
    })
    user.hasMany(models.review, {
      foreignKey: 'userId'
    });
    user.hasMany(models.newReview, {
      foreignKey: 'userId'
    });
  };
  return user;
};