'use strict';
module.exports = (sequelize, DataTypes) => {
  const profile = sequelize.define('profile', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'Please input your name'
        },
      }
    },
    avatar: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {});
  profile.associate = function(models) {
    // associations can be defined here
    profile.belongsTo(models.user, {
      foreignKey: 'user_id'
    })
  };
  return profile;
};