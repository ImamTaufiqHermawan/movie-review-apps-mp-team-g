const router = require("express").Router();

const adminMovie = require("./controller/adminController");

const success = require("./middlewares/success");

router.post("/movies/:movie_id", adminMovie.new, success);

module.exports = router;
