const { Sequelize } = require('sequelize');
const Op = Sequelize.Op
const { movie, review, user, newReview, profile } = require("../models");
const { getMovies, getMovie, searchMovie, getByGenre, getCredits } = require('../lib/tmdb');
const average = require('../helper/averageRatingCounter');
const { response } = require('express');



module.exports = {
    async find(req, res, next) {
        if (req.params.id) {
            try {
                const showMovie = await movie.findByPk(req.params.id, {
                    attributes: {
                        exclude: ["updatedAt", "createdAt"],
                    },
                    include: {
                        model: review,
                        attributes: {
                            exclude: ["updatedAt", "createdAt"],
                        },
                        include: {
                            model: user,
                            attributes: {
                                exclude: [
                                    "role",
                                    "encrypted_password",
                                    "updatedAt",
                                    "createdAt"
                                ],
                            },
                        },
                    },

                });
                res.data = showMovie;
                return next();
            } catch (error) {
                res.statusCode = 400;
                return next(error);
            }
        }
        try {
            const page = req.query.page || 1;
            const allmovie = await movie.findAll();
            res.data = allmovie;
            res.metadata = {};
            next();
        } catch (error) {
            res.statusCode = 400;
            next(error);
        }
    },

    async findAll(req, res, next) {
        const query = req.query;
        try {
            const movies = await movie.findAll({
                // limit: query.limit <= 10 ? query.limit : 10,
                offset: query.offset,
                order: [
                    ['id', 'ASC']
                ]
            })
            res.status(200);
            res.data = { movies };
            next();
        }
        catch (err) {
            res.status(400);
            console.error(err);
            next(err);
        }
    },

    async findGenre(req, res, next) {
        const query = req.query;
        try {
            const movies = await movie.findAll({
                where: {
                    genre: {
                        [Op.iLike]: `%${req.params.genres}%`
                    }
                },
                limit: query.limit <= 10 ? query.limit : 10,
                offset: query.offset,
                order: [
                    ['id', 'ASC']
                ]
            })
            res.status(200);
            res.data = { movies };
            next();
        }
        catch (err) {
            res.status(400);
            console.error(err);
            next(err);
        }
    },


    async findMovies(req, res, next) {
        try {
            const movies = await getMovies(req.query.page)
            res.data = movies
            next()
        }
        catch (err) {
            res.status(400);
            console.error(err);
            next(err);
        }
    },

    async findOne(req, res, next) {
        try {
            const movie = await getMovie(req.params.id)
            const credits = await getCredits(req.params.id)
            const allReview = await newReview.findAll({
                where: {
                    movieId: req.params.id
                },
                raw: true
            })
            const allUserId = await allReview.map(x => x.userId)
            let promises = await profile.findAll({
                where: {
                    user_id: allUserId
                },
                raw: true
            })
            const detailReviews = await allReview.map(detailReview => ({
                ...detailReview,
                detailProfile: promises.find(promise => promise.user_id === detailReview.userId)
            }))
            let avRating = await average(req.params.id)
            const result = { movie, credits, detailReviews, avRating }
            res.data = result
            next()
        }
        catch (err) {
            res.status(400);
            console.error(err);
            next(err);
        }
    },

    async findByGenre(req, res, next) {
        try {
            const movies = await getByGenre(req.params.genre_id, req.query.page)
            res.data = movies
            next()
        }
        catch (err) {
            res.status(400)
            next(err)
        }
    },

    async searchMovie(req, res, next) {
        try {
            const movies = await searchMovie(req.params.query, req.query.page)
            res.data = movies
            next()
        }
        catch (err) {
            res.status(400)
            next(err)
        }
    },


    async review(req, res, next) {
        try {
            const isAlreadyReview = await review.findOne({
                where: { userId: res.user.id, movieId: req.params.id },
            });
            if (isAlreadyReview) {
                res.statusCode = 400;
                return next(new Error("Your review is already existed"));
            }
            await review.create({
                ...req.body,
                movieId: req.params.id,
                userId: res.user.id,
            });
            const response = await movie.findOne({
                where: { id: req.params.id },
                attributes: {
                    exclude: ["updatedAt", "createdAt"],
                },
                include: {
                    model: review,
                    attributes: {
                        exclude: ["updatedAt", "createdAt"],
                    },
                    include: {
                        model: user,
                        attributes: {
                            exclude: [
                                "encrypted_password",
                                "role",
                                "createdAt",
                                "updatedAt"
                            ],
                        },
                    },
                },
            });
            res.data = response;
            res.statusCode = 201;
            next();
        } catch (error) {
            console.log(error)
            res.statusCode = 400;
            next(error);
        }
    },

    async newReview(req, res, next) {
        try {
            const isAlreadyReview = await newReview.findOne({
                where: {
                    userId: req.user.id,
                    movieId: req.params.id
                },
            });
            if (isAlreadyReview) {
                res.statusCode = 400;
                return next(new Error("Your review is already existed"));
            }
            await newReview.create({
                ...req.body,
                movieId: req.params.id,
                userId: req.user.id,
            })
            const movie = await getMovie(req.params.id)
            const credits = await getCredits(req.params.id)
            const allReview = await newReview.findAll({
                where: {
                    movieId: req.params.id
                }
            })
            let avRating = await average(req.params.id)
            const response = { movie, credits, allReview, avRating }

            res.data = response
            next()
        }
        catch (error) {
            console.log(error)
            res.statusCode = 400;
            next(error);
        }
    },
    async editReview(req, res, next) {
        try {
            let { title, description, rating } = req.body

            let dataUpdate = { title, description, rating }

            await newReview.update(dataUpdate, {
                where: {
                    movieId: req.params.id,
                    userId: req.user.id
                }
            })
            const movie = await getMovie(req.params.id)
            const credits = await getCredits(req.params.id)
            const allReview = await newReview.findAll({
                where: {
                    movieId: req.params.id
                }
            })
            let avRating = await average(req.params.id)
            const response = { movie, credits, allReview, avRating }

            res.status(200);
            res.data = {
                message: "Successfully updated",
                data: response
            };
            next();
        }
        catch (error) {
            console.log(error)
            res.statusCode = 400;
            next(error);
        }
    },
    async deleteReview(req, res, next) {
        try {
            await newReview.destroy({
                where: {
                    userId: req.user.id,
                    movieId: req.params.id
                }
            });
            res.status(200);
            res.data = "Successfully deleted";
            next();
        }
        catch (err) {
            res.status(500);
            next(err);
        }
    }
}