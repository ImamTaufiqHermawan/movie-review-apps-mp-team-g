module.exports = {
    post: require('./postController'),
    auth: require('./authController'),
    pages: require('./pagesController')
  }