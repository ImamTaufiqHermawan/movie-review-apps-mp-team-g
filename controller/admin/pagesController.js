const { movie } = require('../../models');

module.exports = {

  auth: {
    login: (req, res) => {
      if (req.isAuthenticated())
        return res.redirect('/admin/home')
      res.status(200).render('posts/login')
    },
    async show(req, res) {
      const post = await movie.findByPk(req.params.id)
      if (!post) return res.status(404).send('404 Not Found')
  
      res.render('show', {
        post
      })
    }
  }
}