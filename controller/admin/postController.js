const { movie } = require('../../models');
module.exports = {
  async login(req, res) {
    res.render('posts/login')
  },

  async home(req, res) {
    const movies = await movie.findAll()
    res.render('homepage', {
      movies
    })
  },

  async new(req, res) {
    res.render('posts/create')
  },

  async create(req, res) {
    movie.create(req.body)
      .then(post => {
        res.redirect('/admin/home')
      })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        }); 
      })
  },

  async edit(req, res) {
    const post = await movie.findByPk(req.params.id)
    if (!post) return res.status(404).send('404 Not Found')

    res.render('posts/edit', {
      post
    })
  },

  async update(req, res) {
    movie.update({
      title: req.body.title,
      tmdb_id: req.body.tmdb_id,
      image_url: req.body.image_url,
      synopsis: req.body.synopsis,
      genre: req.body.genre,
      backdrop_url: req.body.backdrop_url,
      productionCompany: req.body.productionCompany,
      movieDuration: req.body.movieDuration,
      releaseDate: req.body.releaseDate,
      status: req.body.status,
      originalLanguage: req.body.originalLanguage,
      budget: req.body.budget,
      revenue: req.body.revenue,
      averageRating: req.body.averageRating
    }, {
      where: {
        id: req.params.id
      }
    })
    .then(post => {
      res.redirect('/admin/home')
    })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        }); 
      })
  },

  async show(req, res) {
    const post = await movie.findByPk(req.params.id)
    if (!post) return res.status(404).send('404 Not Found')

    res.render('show', {
      post
    })
  }

/*
  async remove(req, res) {
    movie.destroy({
      where: { id: req.params.id }
    })
    .then(post => {
      res.redirect('/admin/home')
    })
      .catch(err => {
        res.status(422).json({
          status: 'fail',
          errors: [err.message]
        }); 
      })
  }
*/

}