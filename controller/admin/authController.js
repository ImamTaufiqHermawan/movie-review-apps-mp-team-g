const { user } = require('../../models')

module.exports = {
  async login(req, res) {
    try {
      await user.authenticate(req.body)
      res.redirect('/admin/login')
    }

    catch(err) {
      res.redirect('/404')
    }
  },

  logout(req, res) {
    req.logOut()
    res.redirect('/admin/login')
  }
}
