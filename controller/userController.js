const { user, profile, newReview } = require('../models');
const { getMovie } = require('../lib/tmdb')
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { review } = require('./movieController');
const { all } = require('../router');
const { response } = require('express');

module.exports = {
    async register(req, res) {
        const hashedPassword = await bcrypt.hash(req.body.password, 10);
        try {
            const newUser = await user.create({
                email: req.body.email,
                encrypted_password: hashedPassword,
                role: req.body.role
            })
            const newUserProfile = await profile.create({
                name: req.body.name,
                user_id: newUser.id
            })
            const token = await jwt.sign({
                id: newUser.id,
                email: newUser.email
            }, process.env.SECRET_KEY)
            res.status(201).json({
                "status": "success",
                data: { newUser, newUserProfile, token }
            })
        }
        catch (err) {
            console.log(err)
            res.status(400).json({
                "status": "failed",
                "message": [err.message]
            })
        }
    },
    async login(req, res) {
        let User = await user.findOne({
            where: {
                email: req.body.email
            }, include: [profile]
        })
        if (User == null) {
            return res.status(400).json({
                "status": "failed",
                "message": "user doesn't exist or email must be fill with lowercase"
            })
        }
        const token = await jwt.sign({
            id: User.id,
            email: User.email
        }, process.env.SECRET_KEY)
        try {
            if (await bcrypt.compare(req.body.password, User.encrypted_password)) {
                res.status(201).json({
                    "status": "success",
                    data: { User, token }
                })
            } else {
                res.status(400).json({
                    "status": "failed",
                    "message": "wrong password"
                })
            }
        }
        catch (err) {
            res.status(500).json({
                "status": "failed",
                "message": [err.message]
            })
        }
    },
    async userData(req, res) {
        try {
            let userData = await user.findByPk(req.user.id, {
                include: [profile]
            });
            res.status(200).json({
                "status": "success",
                data: { userData }
            })
        } catch (err) {
            res.status(500).json({
                status: 'fail',
                message: err
            })
        }
    },
    async UpdatePassword(req, res) {
        try {
            let userID = req.user.id
            const hashedPassword = await bcrypt.hash(req.body.password, 10);

            await user.update({
                encrypted_password: hashedPassword
            }, {
                where: {
                    id: userID
                }
            })
            res.status(200).json({
                "status": "success",
                "message": `successfully update password user ${userID}`
            })
        } catch (err) {
            res.status(422).json({
                status: "fail",
                message: [err.message]
            })
        }
    },
    async userReview(req, res, next) {
        try {
            let userData = await user.findByPk(req.user.id, {
                include: [profile]
            });
            const allReview = await newReview.findAll({
                where: {
                    userId: req.user.id
                },
                raw: true,
            })
            const promises = []
            for (singleReview of allReview) {
                promises.push(getMovie(singleReview.movieId))
            }

            try {
                let responses = await Promise.all(promises);
                responses = responses.map(response => response)
                
                const responsePayload = allReview.map(wkwkwkww => ({
                    ...wkwkwkww,
                    detailMovie: responses.find(response => response.id === wkwkwkww.movieId)
                }))

                console.dir(responsePayload)

                const result = { userData, responsePayload }
                res.data = result
                next()
            } catch (err) {
                console.error(err)
            }
        }
        catch (err) {
            res.status(400);
            console.error(err);
            next(err);
        }
    }
}