const { user, profile } = require('../models');
const imagekit = require('../lib/imagekit');

module.exports = {
    async updateProfile(req, res) {
        const { name } = req.body;
        const file = req.file;
        if (file) {
            const split = await req.file.originalname.split('.');
            const ext = await split[split.length - 1];
            const profilePic = await imagekit.upload({
                file: req.file.buffer,
                fileName: `IMG-${Date.now()}.${ext}`
            })
            try {
                let update = await profile.update({
                    name,
                    avatar: profilePic.url
                }, {
                    where: {
                        user_id: req.user.id
                    }
                })
                res.status(200).json({
                    "status": "success",
                    data: { name, url: profilePic.url }
                })
            } catch (err) {
                res.status(422).json({
                    status: "fail",
                    message: [err.message]
                })
            }
        } else {
            try {
                let update = await profile.update({
                    name,
                    avatar: file
                }, {
                    where: {
                        user_id: req.user.id
                    }
                })
                res.status(200).json({
                    "status": "success",
                    data: { name, file }
                })
            } catch (err) {
                res.status(422).json({
                    status: "fail",
                    message: [err.message]
                })
            }
        }
    }
}