const { movie } = require("../models");

const { searchMovie, getMovie } = require("../lib/tmdb");

module.exports = {
    async find(req, res, next) {
        try {
            const page = req.query.page || 1;
            const fetchData = await searchMovie(req.query.keyword, page);
            res.data = fetchData.data.results;
            res.metadata = {};
            next();
        } catch (error) {
            res.statusCode = 400;
            next(error);
        }
    },

    async new(req, res, next) {
        try {
            const isMovieAlreadyExist = await movie.findOne({
                where: { tmdb_id: req.params.movie_id },
            });
            if (isMovieAlreadyExist) {
                res.statusCode = 400;
                return next(new Error("Movie already exist in database!"));
            }
            const { 
                title, 
                genres, 
                overview, 
                poster_path,
                backdrop_path, 
                production_companies, 
                runtime, 
                release_date, 
                status, 
                original_language, 
                budget, 
                revenue, 
            } = await getMovie(
                req.params.movie_id
            );
            const concatted1 = genres.map(genre => genre.name).join(', ')
            const concatted2 = production_companies.map(production_company => production_company.name).join(', ')

            const createdEntries = await movie.create({
                title,
                synopsis: overview,
                image_url: "http://image.tmdb.org/t/p/w500" + poster_path,
                tmdb_id: req.params.movie_id,
                genre: concatted1,
                backdrop_url: "http://image.tmdb.org/t/p/w500" + backdrop_path,
                productionCompany: concatted2,
                movieDuration: runtime,
                releaseDate: release_date,
                status: status,
                originalLanguage: original_language,
                budget: budget,
                revenue: revenue

            });
            res.data = createdEntries;
            res.statusCode = 201;
            next();
        } catch (error) {
            // console.log(error)
            res.status(400).json({ status: "failed", message: error.message });
        }
    },
};
