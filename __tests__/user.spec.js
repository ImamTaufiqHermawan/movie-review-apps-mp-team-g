const request = require('supertest');
const app = require('../server');
const db = require('../models');
const { user, profile } = require('../models');
const jwt = require('jsonwebtoken')
let token;

describe('User API Collection', () => {
    beforeAll((done) => {
        db.sequelize.query('TRUNCATE "user", "profile" RESTART IDENTITY');
        user.create({
            email: 'coba11@mail.com',
            encrypted_password: '123456'
        }).then(User => {
            profile.create({
                name: "try",
                user_id: User.dataValues.id
            }).then(Profile => {
                token = jwt.sign({
                    id: User.dataValues.id,
                    email: User.dataValues.email
                }, process.env.SECRET_KEY);
                console.log(token)
                done();
            })
        })
    })

    afterAll(() => {
        db.sequelize.query('TRUNCATE "user", "profile" RESTART IDENTITY');
        user.destroy({
            where: {
                email: 'coba11@mail.com' && 'coba12@mail.com'
            },
            truncate: true,
            cascade: true
        })
    })

    describe('POST /api/v1/user/register', () => {
        test('Should successfully create a new user', done => {
            request(app).post('/api/v1/user/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'try again',
                    email: 'coba12@mail.com',
                    password: '123456'
                })
                .then(res => {
                    console.log(token + "==================== success register");
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success');
                    expect(res.body).not.toBeFalsy();
                    done();
                })
        })
        test('Should not created a new account with the same email', done => {
            request(app).post('/api/v1/user/register')
                .set('Content-Type', 'application/json')
                .send({
                    name: 'try',
                    email: 'coba12@mail.com',
                    password: '123456'
                })
                .then(res => {
                    console.log(token + "==================== fail register");
                    expect(res.body.status).toEqual('failed')
                    expect(res.statusCode).toEqual(400)
                    done();
                })
        })
    })

    describe('POST /api/v1/user/login', () => {
        test('user success login', done => {
            request(app).post('/api/v1/user/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: 'coba12@mail.com',
                    password: '123456'
                })
                .then(res => {
                    console.log(token + "==================== success login");
                    expect(res.statusCode).toEqual(201);
                    expect(res.body.status).toEqual('success')
                    done();
                })
        })
        test('user failed login', done => {
            request(app)
                .post('/api/v1/user/login')
                .set('Content-Type', 'application/json')
                .send({
                    email: "tryu@mail.com",
                    password: "12345",
                })
                .then(res => {
                    console.log(token + "==================== fail login");
                    expect(res.statusCode).toEqual(400);
                    expect(res.body.status).toEqual('failed');
                    done();
                })
        })
    })

    describe('PUT /api/v1/user', () => {
        test('Success updated profile', done => {
            request(app)
                .put('/api/v1/user')
                .set('authorization', token)
                .set('Content-Type', 'multipart/form-data')
                .attach('image', './lib/image_example/brad-pitt.jpg')
                .field({
                    name: "cek upload"
                })
                .then(res => {
                    console.log(token + "==================== success update profile");
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('PUT /api/v1/user/updatepassword', () => {
        test('Success updated profile', done => {
            request(app)
                .put('/api/v1/user/updatepassword')
                .set('authorization', token)
                .set('Content-Type', 'application/json')
                .send({
                    password: '123456789'
                })
                .then(res => {
                    console.log(token + "==================== success update password");
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

    describe('PUT /api/v1/user/updatepassword', () => {
        test('failed updated password', done => {
            request(app)
                .put('/api/v1/user/updatepassword')
                .set('authorization', token)
                .set('Content-Type', 'application/json')
                .send({
                    password: null
                })
                .then(res => {
                    console.log(token + "==================== fail update");
                    expect(res.statusCode).toEqual(422);
                    expect(res.body.status).toEqual('fail');
                    done();
                })
        })
    })

    describe('GET /api/v1/user', () => {
        test('success show user data', done => {
            request(app)
                .get('/api/v1/user')
                .set('authorization', token)
                .then(res => {
                    console.log(token + "==================== show user data");
                    expect(res.statusCode).toEqual(200);
                    expect(res.body.status).toEqual('success');
                    done();
                })
        })
    })

})  