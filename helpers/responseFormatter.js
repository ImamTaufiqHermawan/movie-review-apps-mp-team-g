module.exports = {
    successResponse: (data, meta) => {
      return {
        status: 'success',
        meta,
        data,
      }
    },
    
    errorResponse: errors => {
      return {
        status: 'fail',
        errors
      }
    }
}  