const router = require('express').Router();

// Middleware
const passport = require('./lib/passport')
const authenticate = require('./middlewares/authenticate');
const isAdmin = require('./middlewares/isAdmin');
// const checkOwnership = require('./middlewares/checkOwnership');
const upload = require('./middlewares/uploader');
const restrict = require('./middlewares/restrict');
const success = require('./middlewares/success');

// Controllers
const admin = require('./controller/admin')
const userController = require('./controller/userController');
const profileController = require('./controller/profileController');
const movie = require('./controller/movieController')

// Old
router.get("/api/v1/movies/", movie.findAll, success);
router.get("/api/v1/movies/:genres", movie.findGenre, success);
router.get("/api/v1/movies/id/:id?", movie.find, success);
router.post("/api/v1/movies/:id/review", authenticate, movie.review, success);

// Movie Collection API
router.get("/api/v1/dmovies/", movie.findMovies, success)
router.get("/api/v1/dmovies/:id", movie.findOne, success)
router.get("/api/v1/dmovies/genre/:genre_id", movie.findByGenre, success)
router.get("/api/v1/dmovies/search/:query", movie.searchMovie, success)
router.post("/api/v1/dmovies/:id/review", authenticate, movie.newReview, success);
router.put("/api/v1/dmovies/:id/review", authenticate, movie.editReview, success)
router.delete("/api/v1/dmovies/:id/review", authenticate, movie.deleteReview, success)

//user API 
router.post('/api/v1/user/register', userController.register);
router.post('/api/v1/user/login', userController.login);
router.get('/api/v1/user/review', authenticate, userController.userReview, success)
router.put('/api/v1/user/updatepassword', authenticate, userController.UpdatePassword);
router.put('/api/v1/user', authenticate, upload.single('image'), profileController.updateProfile);
router.get('/api/v1/user', authenticate, userController.userData);

//server side rendering
router.use(passport.initialize())
router.use(passport.session())
//router.post('/api/v1/auth/register', auth.register)
router.get('/admin/login', admin.pages.auth.login)
router.get('/admin/home', restrict,  admin.post.home)
router.get('/admin/movie/new', restrict, admin.post.new)
router.post('/admin/movie/new', admin.post.create)
router.get('/admin/movie/edit/:id', restrict, admin.post.edit)
router.post('/admin/movie/edit/:id', admin.post.update)
router.get('/admin/movie/:id', restrict, admin.post.show)
router.post(
    '/admin/login',
    passport.authenticate('local', {
      successRedirect: '/admin/home',
      failureRedirect: '/admin/login',
      failureFlash: true
    })
)
router.get('/logout', admin.auth.logout)

module.exports = router;