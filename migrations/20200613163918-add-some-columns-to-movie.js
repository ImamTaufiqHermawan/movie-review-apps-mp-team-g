'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.addColumn('movies', 'backdrop_url', {
          type: Sequelize.DataTypes.STRING,
        }, { transaction: t }),
        queryInterface.addColumn('movies', 'productionCompany', {
          type: Sequelize.DataTypes.STRING,
        }, { transaction: t }),
        queryInterface.addColumn('movies', 'movieDuration', {
          type: Sequelize.DataTypes.STRING
        }, { transaction: t }),
        queryInterface.addColumn('movies', 'releaseDate', {
          type: Sequelize.DataTypes.STRING
        }, { transaction: t }),
        queryInterface.addColumn('movies', 'status', {
          type: Sequelize.DataTypes.STRING
        }, { transaction: t }),
        queryInterface.addColumn('movies', 'originalLanguage', {
          type: Sequelize.DataTypes.STRING,
        }, { transaction: t }),
        queryInterface.addColumn('movies', 'budget', {
          type: Sequelize.DataTypes.STRING,
        }, { transaction: t }),
        queryInterface.addColumn('movies', 'revenue', {
          type: Sequelize.DataTypes.STRING,
        }, { transaction: t }),
        queryInterface.addColumn('movies', 'averageRating', {
          type: Sequelize.DataTypes.INTEGER,
        }, { transaction: t }),
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction(t => {
      return Promise.all([
        queryInterface.removeColumn('movies', 'backdrop_url', { transaction: t }),
        queryInterface.removeColumn('movies', 'productionCompany', { transaction: t }),
        queryInterface.removeColumn('movies', 'movieDuration', { transaction: t }),
        queryInterface.removeColumn('movies', 'releaseDate', { transaction: t }),
        queryInterface.removeColumn('movies', 'status', { transaction: t }),
        queryInterface.removeColumn('movies', 'originalLanguage', { transaction: t }),
        queryInterface.removeColumn('movies', 'budget', { transaction: t }),
        queryInterface.removeColumn('movies', 'revenue', { transaction: t }),
        queryInterface.removeColumn('movies', 'averageRating', { transaction: t }),
      ]);
    });
  }
};
